// https://dave.cheney.net/2013/06/30/how-to-write-benchmarks-in-go
package bench

import (
	"math/rand"
	"testing"
	"time"
)

func randomInts(len int) []int {
	rand.Seed(time.Now().Unix())
	out := make([]int, len)

	for i := 0; i < len; i++ {
		out = append(out, rand.Int())
	}

	return out
}

func BenchmarkBubbleSort1(b *testing.B) {
	// run the Fib function b.N times
	for n := 0; n < b.N; n++ {
		bubbleSort(randomInts(1))
	}
}
func BenchmarkBubbleSort10(b *testing.B) {
	// run the Fib function b.N times
	for n := 0; n < b.N; n++ {
		bubbleSort(randomInts(10))
	}
}
func BenchmarkBubbleSort100(b *testing.B) {
	// run the Fib function b.N times
	for n := 0; n < b.N; n++ {
		bubbleSort(randomInts(100))
	}
}

func BenchmarkInsertionSort1(b *testing.B) {
	// run the Fib function b.N times
	for n := 0; n < b.N; n++ {
		insertionSort(randomInts(1))
	}
}
func BenchmarkInsertionSort10(b *testing.B) {
	// run the Fib function b.N times
	for n := 0; n < b.N; n++ {
		insertionSort(randomInts(10))
	}
}
func BenchmarkInsertionSort100(b *testing.B) {
	// run the Fib function b.N times
	for n := 0; n < b.N; n++ {
		insertionSort(randomInts(100))
	}
}

func BenchmarkMergeSort1(b *testing.B) {
	// run the Fib function b.N times
	for n := 0; n < b.N; n++ {
		mergeSort(randomInts(1))
	}
}
func BenchmarkMergeSort10(b *testing.B) {
	// run the Fib function b.N times
	for n := 0; n < b.N; n++ {
		mergeSort(randomInts(10))
	}
}
func BenchmarkMergeSort100(b *testing.B) {
	// run the Fib function b.N times
	for n := 0; n < b.N; n++ {
		mergeSort(randomInts(100))
	}
}
