package bench

func bubbleSort(a []int) {
	for itemCount := len(a) - 1; ; itemCount-- {
		hasChanged := false
		for index := 0; index < itemCount; index++ {
			if a[index] > a[index+1] {
				a[index], a[index+1] = a[index+1], a[index]
				hasChanged = true
			}
		}
		if hasChanged == false {
			break
		}
	}
}

func insertionSort(a []int) {
	for i := 1; i < len(a); i++ {
		value := a[i]
		j := i - 1
		for j >= 0 && a[j] > value {
			a[j+1] = a[j]
			j = j - 1
		}
		a[j+1] = value
	}
}

func mergeSort(a []int) {
	var s = make([]int, len(a)/2+1) // scratch space for merge step

	if len(a) < 2 {
		return
	}
	mid := len(a) / 2
	mergeSort(a[:mid])
	mergeSort(a[mid:])
	if a[mid-1] <= a[mid] {
		return
	}
	// merge step, with the copy-half optimization
	copy(s, a[:mid])
	l, r := 0, mid
	for i := 0; ; i++ {
		if s[l] <= a[r] {
			a[i] = s[l]
			l++
			if l == mid {
				break
			}
		} else {
			a[i] = a[r]
			r++
			if r == len(a) {
				copy(a[i+1:], s[l:mid])
				break
			}
		}
	}
	return
}
