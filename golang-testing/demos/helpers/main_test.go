package main

import (
	"io/ioutil"
	"testing"
)

func loadConfig(t *testing.T) *Config {
	t.Helper()
	data, err := ioutil.ReadFile("/path/to/the/thing")
	if err != nil {
		t.Fail()
	}

	return &Config{data}
}

func TestSomething(t *testing.T) {
	t.Run("should pass", func(_t *testing.T) {
	})
	t.Run("should fail", func(_t *testing.T) {
		loadConfig(_t)
	})
}
