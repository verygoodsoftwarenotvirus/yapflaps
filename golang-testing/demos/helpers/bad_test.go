package main

import (
	"io/ioutil"
	"testing"
)

func loadExampleConfig(t *testing.T) *Config {
	data, err := ioutil.ReadFile("/path/to/the/thing")
	if err != nil {
		t.Fail()
	}

	return &Config{data}
}
