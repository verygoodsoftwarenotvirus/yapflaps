package main

import (
	"testing"
)

func TestThisAlwaysRunsFirst(t *testing.T) {
	t.Run("these", func(t2 *testing.T) { t2.Parallel() })
	t.Run("will", func(t2 *testing.T) { t2.Parallel() })
	t.Run("run", func(t2 *testing.T) { t2.Parallel() })
	t.Run("whenever", func(t2 *testing.T) { t2.Parallel() })
}

func TestThisAlwaysRunsSecond(t *testing.T) {
	t.Run("these", func(t2 *testing.T) { t2.Parallel() })
	t.Run("will", func(t2 *testing.T) { t2.Parallel() })
	t.Run("run", func(t2 *testing.T) { t2.Parallel() })
	t.Run("whenever", func(t2 *testing.T) { t2.Parallel() })
}

func TestThisAlwaysRunsThird(t *testing.T) {
	t.Run("these", func(t2 *testing.T) { t2.Parallel() })
	t.Run("will", func(t2 *testing.T) { t2.Parallel() })
	t.Run("run", func(t2 *testing.T) { t2.Parallel() })
	t.Run("whenever", func(t2 *testing.T) { t2.Parallel() })
}
