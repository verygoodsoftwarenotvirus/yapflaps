package example

import (
	"testing"
)

func TestIsEven(t *testing.T) {
	if IsEven(1) || !IsEven(2) {
		t.FailNow()
	}
}
