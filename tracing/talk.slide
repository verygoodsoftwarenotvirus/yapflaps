Distributed Tracing in Go

Austin Go Meetup April 2019

Jeffrey Dorrycott
jeffrey.dorrycott@wpengine.com

* About me

- I work at WP Engine on one of our distributed systems teams
- I've been writing Go for the last 4 years-ish

.link https://blog.verygoodsoftwarenotvirus.ru

.link https://gitlab.com/verygoodsoftwarenotvirus/yapflaps

: I've had four jobs as a software engineer, and at each job have managed to earn the reputation of Go zealot. So when old coworkers ask me why WP Engine would ever use Go, it's always really fun, because we use Go for a ton of things.

* An example system

    +-------------------------------------+
    |                                     |
    |                          +----+     |
    |                          | DB |     |
    |                          +----+     |
    |                           |  |      |
    |                           |  |      |
    |   +-------+           +---+--+---+  |
    |   |       |           |          |  |
    |   | Users |<--------->+  Server  |  |
    |   |       |           |          |  |
    |   +-------+           +----------+  |
    |                                     |
    +-------------------------------------+


: Lets say you have a product you've built a company around, and you've got a super simple web app that's making customers happy. You run one machine with one database instance, and all works well. You focus on non-infrastructural tasks becuase you're at an earlier stage in your company's lifecycle.

* An example system

    +---------------------------------------------------+
    |                          +----+                   |
    |                  +-------+ DB +-------+           |
    |                  |       +-+--+       |           |
    |                  |         |          |           |
    |          +-------+--+ +----+-----+ +--+-------+   |
    |          |          | |          | |          |   |
    |          |  Server  | |  Server  | |  Server  |   |
    |          |          | |          | |          |   |
    |          +-----+----+ +----+-----+ +-----+----+   |
    |                |           |             |        |
    |                |       +---+----+        |        |
    |                +-------+  Load  +--------+        |
    |                        |Balancer|                 |
    |                        +--------+                 |
    |     +-------+              ^                      |
    |     |       |              |                      |
    |     | Users +--------------+                      |
    |     |       |                                     |
    |     +-------+                                     |
    +---------------------------------------------------+

: A while goes by, more customers sign up and the old infra doesn't cut it anymore, so you add a bunch of machines together.

* An example system

    +---------------------------------------------------------------------------------+
    |                        +-----------------------------+                          |
    |                        |                    |        |         +------------+   |
    |                      +-+--+                 |  +-----+-----+   |            |   |
    |              +-------+ DB +-------+         |  |           +-->+ +--------+ |   |
    |              |       +-+--+       |         |  |    ETL    |   | |        | |   |
    |              |         |          |         |  |           <---+ | Worker | |   |
    |      +-------+--+ +----+-----+ +--+-------+ |  +-----------+   | |        | |   |
    |      |          | |          | |          | |                  | +--------+ |   |
    |      |  Server  | |  Server  | |  Server  | |                  |            |   |
    |      |          | |          | |          | |                  | +--------+ |   |
    |      +-----+----+ +----+-----+ +-----+----+ |                  | |        | |   |
    |            |           |             |      |                  | | Worker | |   |
    |            |       +---+----+        |      |                  | |        | |   |
    |            +-------+  Load  +--------+      |                  | +--------+ |   |
    |                    |Balancer|               |                  |            |   |
    |                    +---+----+               |                  | +--------+ |   |
    | +-------+              ^                    |                  | |        | |   |
    | |       |              |                    +------------------+ | Worker | |   |
    | | Users +--------------+                                       | |        | |   |
    | |       |                                                      | +--------+ |   |
    | +-------+                                                      |    ...     |   |
    |                                                                +------------+   |
    +---------------------------------------------------------------------------------+

: Time goes by, your business and your system grows and evolves, and now you have an ETL system that transforms a user's initial upload into something significantly different from what they put in. Not to mention, maybe you didn't expose a good backend API to the ETL team, so they're writing straight to the database. If a customer complains that your site is slow, how can you find out

* History of the problem

- If there are thousands of machines that could be responsible for
- at some scale, performance degradation in a single system can cause performance degradation in dozens of others.
- distributed systems aren't new, but some distributed systems exist at newly large sizes

: when your customers' interactions touch a few machines, common tooling easily accommodates bug hunting
: when your customers' interactions touch a few hundred machines, more advanced tooling is required to diagnose more thoroughly.
: General Systemantics Law of Growth: "systems tend to grow, and as they grow, they encroach".

* Lower level: local application tracing in Go

- runtime/trace

: runtime/trace provides mechanisms for tying tasks together causally

- golang.org/x/net/trace for network level tracing

: net/trace provides details on things like connection start time and TLS handshake start/finish times

* Basic Language

: As I understand it, most of these originate in the Dapper whitepaper, but they're not exactly revolutionary terms

- traces

: A trace is "a data/execution path through the system, and can be thought of as a directed acyclic graph of spans."

- spans

: "A span represents a logical unit of work in Jaeger that has an operation name, the start time of the operation, and the duration. Spans may be nested and ordered to model causal relationships."
: `runtime/trace` calls spans "regions" and traces "tasks"

- annotations (baggage)

: Items added to the trace to provide additional debugging context. Something like a request ID or database hostname used to query results

.image images/terminology.png 500 700

* Dapper

- closed-source internal system at Google
- instrumented into lower-level libraries that had mandatory adoption within the org
- extreme minority of services unavailable
.link https://storage.googleapis.com/pub-tools-public-publication-data/pdf/36356.pdf Dapper paper

: “In many of the larger systems at Google, it is not uncommon to find traces with thousands of spans.”
: One cool thing about Dapper is that they send 1/1024 samples they collect and are still able to observe issues
: "There are cases where Dapper is unable to follow the control path correctly. Presently there are 40 C++ applications and 33 Java applications that required some manual trace propagation, corresponding to a small fraction of the totals which number in the thousands. There is also a very small number of programs that use uninstrumented communication libraries (raw TCP sockets, or SOAP RPCs, for example), and therefore do not support Dapper tracing."

* Jaeger's Rollout

.image images/jaeger_dist.png 500 800

: To demonstrate how incredible it is that Dapper got permeated through basically all of Google so quickly, here's how Uber, who has entire teams who work on distributed tracing infrastructure and libraries, has been able to penetrate their own microservices with tracing instrumentation.
: Yuri Shkuro, Uber

* Modern art

.image images/modern_art.jpg

* Jaeger

.image images/jaeger.png 500 1000

: Jaeger is written in Go, by Uber. CNCF project, really cool.

* Zipkin

.image images/zipkin.png 500 1000

* Stackdriver Trace

.image images/stackdriver_trace.png 700 800

* AWS X-Ray

.image images/x-ray.png 250 1000

: Also a noticeably more active repository on Github than Jaeger
: Jaeger's docs explicitly call out that they interoperate with Zipkin

* Open Source Distributed Tracing Server Client Libraries

: If you thought you could just pick a server, you also need to pick a library.
: The kicker here is you can use a generic library to connect to a vendor-specific server, so if you want to switch from Jaeger to Zipkin to Stackdriver, it's not as difficult as you might think

.link opentracing.io

: OpenTracing is a very community-oriented project
: "A subset of these tracing system authors are part of the OpenTracing Specification Council (OTSC)."
: "Said council members have write access across the github.com/opentracing and github.com/opentracing-contrib organizations [...]. Each OTSC member represents a tracing system which (a) maintains a public OpenTracing implementation, and (b) has substantial institutional support, and thus longevity."

.link opencensus.io

: "OpenCensus originates from Google, where a set of libraries called Census were used to automatically capture traces and metrics from services. Since going open source, the project is now composed of a group of cloud providers, application performance management vendors, and open source contributors."
: Basically I read that as "We really wanted to open up how to use Stackdriver and decided to put the ball in our competitors' courts as well," which I'm honestly cool with.

* What it looks like to construct a tracer

.code code/initializing_jaeger.go

* Passive tracing implementation

.code code/opentracing_middleware.go

: Note here that you wouldn't actually want to concat the path as most APIs have variable routes

* Active tracing implementation

.code code/traced_code.go

* What Jaeger looks like

* Prometheus vs Jaeger

- Prometheus is a time-series database
- can aggregate data about a system
- not tooled for analyzing behavior across systems

: Jaeger's CNCF FAQ: "While traditional monitoring tools still have their place, they often fail to provide visibility _across services_. This is where distributed tracing, namely Jaeger, thrives."
