nethttp.Middleware(
	s.tracer,
	next,
	nethttp.MWComponentName("traced-service"),
	nethttp.MWSpanObserver(func(span opentracing.Span, req *http.Request) {
		span.SetTag("http.method", req.Method)
		span.SetTag("http.uri", req.URL.EscapedPath())
	}),
	nethttp.OperationNameFunc(func(req *http.Request) string {
		return fmt.Sprintf("%s %s", req.Method, req.URL.Path)
	}),
)
