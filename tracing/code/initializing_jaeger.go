import (
	"github.com/opentracing/opentracing-go"
	"github.com/uber/jaeger-client-go/config"
	"github.com/uber/jaeger-lib/metrics/expvar"
)

// ProvideTracer provides a tracer
func ProvideTracer(service string) (opentracing.Tracer, error) {
	cfg, err := config.FromEnv()
	if err != nil {
		return nil, err
	}
	cfg.ServiceName, cfg.Sampler.Type, cfg.Sampler.Param = service, "const", 1

	metricsFactory := expvar.NewFactory(10).Namespace(cfg.ServiceName, nil)
	tracer, _, err := cfg.NewTracer(
		config.Metrics(metricsFactory),
	)
	if err != nil {
		return nil, err
	}

	return tracer, nil
}

