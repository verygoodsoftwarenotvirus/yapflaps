// GetItem fetches an item from the database
func (c *Client) GetItem(ctx context.Context, itemID, userID uint64) (*models.Item, error) {
	span := tracing.FetchSpanFromContext(ctx, c.tracer, "GetItem")
	span.SetTag("item_id", itemID)
	span.SetTag("user_id", userID)
	ctx = opentracing.ContextWithSpan(ctx, span)
	defer span.Finish()

	c.logger.WithValues(map[string]interface{}{
		"item_id": itemID,
		"user_id": userID,
	}).Debug("GetItem called")

	return c.database.GetItem(ctx, itemID, userID)
}

