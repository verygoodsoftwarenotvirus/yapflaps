func loadPlugin(pluginPath string, symbolName string) (plugin.Symbol, error) {
	if pluginPath == "" {
		return nil, errors.New("plugin path cannot be empty")
	}
	if symbolName == "" {
		return nil, errors.New("symbol name cannot be empty")
	}

	p, err := plugin.Open(pluginPath)
	if err != nil {
		return nil, errors.Wrap(err, "failed to open plugin")
	}

	sym, err := p.Lookup(symbolName)
	if err != nil {
		return nil, errors.Wrap(err, "failed to locate appropriate plugin symbol")
	}
	return sym, nil
}
