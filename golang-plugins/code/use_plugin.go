func loadDatabasePlugin(pluginPath string, name string) (database.Storer, error) {
	dbSym, err := loadPlugin(pluginPath, name)
	if err != nil {
		return nil, errors.Wrap(err, "failed to load plugin")
	}
	if _, ok := dbSym.(database.Storer); !ok {
		return nil, errors.New("Symbol provided in database plugin does not satisfy the database.Storer interface")
	}

	return dbSym.(database.Storer), nil
}
