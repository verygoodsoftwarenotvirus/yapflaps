package main

import (
	"io"
	"net/http"
)

func handlerFunc(res http.ResponseWriter, req *http.Request) {
	var (
		ids   []uint
		names []string
	)
	rows, err := db.Query("some query", "some args")
	if err != nil {
		io.WriteString(res, "something done goofed")
	}
	defer rows.Close()
	for rows.Next() {
		var (
			id   uint
			name string
		)
		err := rows.Scan(&id, &name)
		if err != nil {
			io.WriteString(res, "error scanning row")
		}
		ids = append(ids, id)
		names = append(names, name)
	}
}
