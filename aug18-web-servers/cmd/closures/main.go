package main

import (
	"database/sql"
	"net/http"
)

var (
	db *sql.DB
)

func main() {
	http.HandleFunc("/", handlerFunc)

	http.ListenAndServe(":8080", nil)
}
