package main

import (
	"encoding/json"
	"net/http"
)

func jsonHandler(res http.ResponseWriter, req *http.Request) {
	if req.Method != "GET" {
		return
	}
	json.NewEncoder(res).Encode(someObject)
}
