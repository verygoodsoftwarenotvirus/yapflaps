package main

import (
	"html/template"
	"net/http"
)

func handler(w http.ResponseWriter, r *http.Request) {
	t := template.New("template")
	t, _ = t.ParseFiles("roster.html")
	users := GetUsers()
	t.Execute(w, users)
}
