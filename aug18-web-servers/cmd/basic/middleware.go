package main

import (
	"log"
	"net/http"
)

func Logger(h http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		log.Println("Before serving request")
		h.ServeHTTP(w, r)
		log.Println("After serving request")
	})
}
