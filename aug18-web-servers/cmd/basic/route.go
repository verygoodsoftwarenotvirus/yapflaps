package main

import (
	"io"
	"net/http"
)

func helloHandler(res http.ResponseWriter, req *http.Request) {
	if req.Method != "GET" {
		return
	}
	io.WriteString(res, "Hello world!")
}
