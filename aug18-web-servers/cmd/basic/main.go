package main

import (
	"io"
	"net/http"
)

func main() {
	http.HandleFunc("/", func(res http.ResponseWriter, req *http.Request) {
		if req.Method != "GET" {
			return
		}
		io.WriteString(res, "Hello world!")
	})

	http.ListenAndServe(":8080", nil)
}
