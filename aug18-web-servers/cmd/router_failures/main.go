package main

import (
	"io"
	"net/http"
)

func main() {
	http.HandleFunc("/things/", func(res http.ResponseWriter, req *http.Request) {
		io.WriteString(res, "Things!")
	})

	http.ListenAndServe(":8080", nil)
}
