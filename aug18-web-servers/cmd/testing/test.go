package server

import (
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestHandleAbout(t *testing.T) {
	srv := server{
		db:    mockDatabase,
		email: mockEmailSender,
	}
	srv.routes()

	req, err := http.NewRequest("GET", "/about", nil)
	assert.NoError(err)

	w := httptest.NewRecorder()
	srv.ServeHTTP(w, r)

	assert.Equal(w.StatusCode, http.StatusOK)
}
