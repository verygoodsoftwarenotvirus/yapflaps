package main

import (
	"fmt"
	"time"

	"github.com/dennwc/dom"
)

func dollarSignShow(e *dom.Element) {
	fmt.Println("dollarSignShow called")
	e.SetAttribute("style", "display: block;")
}

func toggleKeyForm() {
	fmt.Println("toggleKeyForm called")
	document := dom.GetDocument()

	// $form = $("#new-key-container")
	newKeyForm := document.GetElementById("new-key-container")

	// $("#new-key-btn").on 'click', ->
	// $form.show()
	// $("#add-ssh-key-btn").prop('disabled', false)
	// $form.get(0).scrollIntoView() /* not gonna do this one */
	newKeyButton := document.GetElementById("new-key-btn")
	newKeyButton.OnClick(func(e *dom.MouseEvent) {
		fmt.Println("newKeyButton called")
		dollarSignShow(newKeyForm)
		document.GetElementById("add-ssh-key-btn").SetAttribute("disabled", false)
	})
}

func nilCheck(e *dom.Element, msg string) {
	if e == nil {
		panic(msg)
	}
}

func wait() {
	for {
		select {
		case <-time.NewTicker(2<<30 - 1*time.Second).C:
		}
	}
}

func main() {
	document := dom.GetDocument()

	newKeyForm := document.GetElementById("new-key-container")
	nilCheck(newKeyForm, "new-key-container")
	newKeyButton := document.GetElementById("new-key-btn")
	nilCheck(newKeyButton, "new-key-btn")

	newKeyButton.OnClick(func(e *dom.MouseEvent) {
		fmt.Println("newKeyButton called")
		dollarSignShow(newKeyForm)
	})

	wait()
}
