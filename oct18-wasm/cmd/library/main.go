//+build wasm

package main

import (
	"fmt"

	"github.com/dennwc/dom"
)

func main() {
	fmt.Println("getting document")
	document := dom.GetDocument()
	fmt.Printf("got document: %v\n", document)

	fmt.Println("getting body")
	body := document.GetElementsByTagName("body")[0]
	fmt.Printf("got body: %v\n", body)
	// body.SetInnerHTML(`<div style="width: 500px; height: 500px;">FUCK</div>`)

	fmt.Println("creating div")
	arbitrary := document.CreateElement("div")
	arbitrary.SetAttribute("style", "width: 500px; height: 500px; background-color: red;")
	fmt.Printf("created div: %v\n", arbitrary)

	fmt.Println("appending child")
	body.AppendChild(arbitrary)
	fmt.Println("getting body")
}
