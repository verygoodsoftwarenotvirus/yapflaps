// +build wasm

package main

import (
	"fmt"
	"syscall/js"
)

const (
	squareStyle = "width: 500px; height: 500px; background-color: red;"
)

var dom = js.Global()

func checkValue(v js.Value, name string) {
	if v == js.Undefined() || v == js.Undefined() {
		panic(fmt.Sprintf("invalid value: %q!", name))
	}
}

func main() {
	fmt.Println("getting document")
	document := dom.Get("document")
	checkValue(document, "document")
	fmt.Printf("got document: %v\n", document)

	fmt.Println("getting body")
	v := document.Call("getElementsByTagName", "body")
	checkValue(v, "body")
	body := v.Index(0)
	fmt.Printf("got body: %v\n", body)

	fmt.Println("creating div")
	div := document.Call("createElement", "div")
	checkValue(v, "name")
	div.Call("setAttribute", "style", squareStyle)
	fmt.Printf("created div: %v\n", div)

	fmt.Println("appending child")
	body.Call("appendChild", div)
	fmt.Println("getting body")
}
